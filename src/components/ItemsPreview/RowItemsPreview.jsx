import React        from 'react';
import PropTypes    from 'prop-types';
import ItemPreview  from './ItemPreview';
import { Row, Col } from 'react-bootstrap';
import VendorFilter from '../VendorFilter';

const RowItemsPreview = ({ items = [], vendors, onAllVendor, onVendorChange }) => (
  <Row>
    <VendorFilter vendors={vendors} onAllVendor={onAllVendor} onVendorChange={onVendorChange} />
    <Col sm={10}>
      {
        items.map((item, index) => {
          return (
            <Col key={index} md={6}>
              <ItemPreview item={item}/>
            </Col>
          );
        })
      }
    </Col>
  </Row>
);

RowItemsPreview.propTypes = {
  onVendorChange: PropTypes.func.isRequired,
  onAllVendor: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.object.isRequired
  ).isRequired,
  vendors: PropTypes.arrayOf(
    PropTypes.string.isRequired
  ).isRequired
};

export default RowItemsPreview;

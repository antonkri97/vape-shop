import React     from 'react';
import PropTypes from 'prop-types';
import Image     from 'react-bootstrap/lib/Image';
import { Link }  from 'react-router';

const ItemPreview = ({ item }) => (
  <div>
    <h3>
      {item.name}
      <br/>
      <small>{item.vendor}</small>
    </h3>
    <Link to={`/item/${item._id}`}>
      <Image src={item.photo.medium} title={item.name}/>
    </Link>
  </div>
);

ItemPreview.propTypes = {
  item: PropTypes.object.isRequired
};

export default ItemPreview;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Navbar, FormGroup, FormControl, Button } from 'react-bootstrap';

class Search extends Component {
  state = {
    value: '',
    validationState: null
  };

  handleChange = (e) => {
    e.preventDefault();
    this.setState({
      value: e.target.value
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.value === '') {
      this.setState({
        validationState: 'warning'
      });
      return;
    }
    this.setState({
      validationState: null
    });

    this.props.onSearch(this.state.value);
  }

  render() {
    return (
      <Navbar.Form pullLeft>
        <FormGroup validationState={this.state.validationState}>
          <FormControl type='text' placeholder='Search' onChange={this.handleChange}/>
          {' '}
          <Link to='/'>
            <Button type='submit' onClick={this.handleSubmit}>
              Search
            </Button>
          </Link>
        </FormGroup>
      </Navbar.Form>
    );
  }
}

Search.propTypes = {
  onSearch: PropTypes.func.isRequired
};

export default Search;

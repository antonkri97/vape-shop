import React     from 'react';
import PropTypes from 'prop-types';
import Button    from 'react-bootstrap/lib/Button';
import { Link }  from 'react-router';

const style = {
  marginTop: '5%',
  marginLeft: '10%'
};

const ToFinalCart = ({ show, onOrderStart }) => (
  <div style={style}>
    { show &&
      <Link to='/final'>
        <Button onClick={onOrderStart}>Оформить заказ</Button>
      </Link>
    }
  </div>
);

ToFinalCart.propTypes = {
  show: PropTypes.bool.isRequired,
  onOrderStart: PropTypes.func.isRequired
};

export default ToFinalCart;

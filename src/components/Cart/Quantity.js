import React     from 'react';
import PropTypes from 'prop-types';
import Image     from 'react-bootstrap/lib/Image';

const Quantity = ({ onQuantityClick, positive, _id, option }) => {
  const css = positive ? 'right' : 'left';
  const image = positive ? 'add' : 'substract';

  return (
    <button className={`qty-button qty-button-${css}`} onClick={() => {
      onQuantityClick(_id, option);
    }}
    >
      <Image className='qty-svg' src={`https://s3.eu-central-1.amazonaws.com/vapeshopimg/icon/${image}.svg`} />
    </button>
  );
};

Quantity.propTypes = {
  onQuantityClick: PropTypes.func.isRequired,
  positive: PropTypes.bool.isRequired,
  _id: PropTypes.string.isRequired,
  option: PropTypes.number.isRequired
};

export default Quantity;

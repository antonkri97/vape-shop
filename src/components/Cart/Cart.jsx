import React     from 'react';
import PropTypes from 'prop-types';
import Item      from './CartItem';
import Forward   from '../common/Forward';

const Cart = ({ cart, onRemoveQuantity, onAddQuantity, nextUrl, onOrderStart, onConfirm }) => (
  <div>
    <h2>
      Корзина
    </h2>
    {
      cart.map((item, index) => {
        return (
          <Item
            item={item}
            onRemoveQuantity={onRemoveQuantity}
            onAddQuantity={onAddQuantity}
            key={index}
          />
        );
      })
    }
    { Boolean(cart.length) && <Forward onForward={onOrderStart} nextUrl={nextUrl} onConfirm={onConfirm}/>}
  </div>
);

Cart.propTypes = {
  cart: PropTypes.arrayOf(
    PropTypes.object.isRequired
  ).isRequired,
  onRemoveQuantity: PropTypes.func.isRequired,
  onAddQuantity: PropTypes.func.isRequired,
  onOrderStart: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  nextUrl: PropTypes.string.isRequired
};

export default Cart;

import React     from 'react';
import PropTypes from 'prop-types';
import Row       from 'react-bootstrap/lib/Row';
import Col       from 'react-bootstrap/lib/Col';
import Quantity  from './Quantity';

import '../../style/Cart.css';

const CartItem = ({ item, onRemoveQuantity, onAddQuantity }) => (
  <Row>
    <Col xs={5} md={4}>
      <h3>
        {item.name}
        <br />
        <small>
          ({item.options[item.option].nicotine}мг, {item.options[item.option].capacity}мл)
        </small>
      </h3>
    </Col>
    <Col xs={4} md={4}>
      <h3>
        <Quantity positive={false} onQuantityClick={onRemoveQuantity} _id={item._id}
          option={item.option}
        />
        {item.quantity}
        <Quantity positive onQuantityClick={onAddQuantity} _id={item._id}
          option={item.option}
        />
      </h3>
    </Col>
    <Col xs={3} md={4}>
      <h3>
        {item.options[item.option].price * item.quantity}
        <br />
        <small>{item.options[item.option].price}</small>
      </h3>
    </Col>
  </Row>
);

CartItem.propTypes = {
  item: PropTypes.object.isRequired,
  onRemoveQuantity: PropTypes.func.isRequired,
  onAddQuantity: PropTypes.func.isRequired
};

export default CartItem;

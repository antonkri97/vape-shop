import React                        from 'react';
import PropTypes                    from 'prop-types';
import { Col, ButtonGroup, Button } from 'react-bootstrap';

const VendorFilter = ({ vendors = [], onAllVendor, onVendorChange }) => (
  <Col sm={2}>
    <ButtonGroup vertical>
      <Button onClick={onAllVendor}>
        Все производители
      </Button>
      {
        vendors.map((vendor, index) => (
          <Button key={index} onClick={() => onVendorChange(vendor)}>
            {vendor}
          </Button>
        ))
      }
    </ButtonGroup>
  </Col>
);

VendorFilter.propTypes = {
  vendors: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
  onAllVendor: PropTypes.func.isRequired,
  onVendorChange: PropTypes.func.isRequired
};

export default VendorFilter;

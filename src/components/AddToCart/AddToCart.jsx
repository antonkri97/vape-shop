import React     from 'react';
import PropTypes from 'prop-types';
import Button    from 'react-bootstrap/lib/Button';
import { check } from '../../utils/cart';

const AddToCart = ({ id, option, itemInCart, onAddToCartClick }) => {
  const alredyInCart = check(option, itemInCart);

  return (
    <Button onClick={() => onAddToCartClick(id, option)} disabled={alredyInCart}>
      {alredyInCart ? 'В корзине' : 'Добавить в корзину'}
    </Button>
  );
};

AddToCart.propTypes = {
  onAddToCartClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  option: PropTypes.number.isRequired,
  itemInCart: PropTypes.arrayOf(PropTypes.number).isRequired,
  confirmed: PropTypes.bool.isRequired
};

export default AddToCart;

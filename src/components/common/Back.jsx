import React      from 'react';
import PropTypes  from 'prop-types';
import { Button } from 'react-bootstrap';
import { Link }   from 'react-router';

const Back = ({ onBack, nextUrl }) => (
  <Link to={nextUrl}>
    <Button onClick={onBack}>
      Назад
    </Button>
  </Link>
);

Back.propTypes = {
  onBack: PropTypes.func.isRequired,
  nextUrl: PropTypes.string.isRequired
};

export default Back;

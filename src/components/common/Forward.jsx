import React      from 'react';
import PropTypes  from 'prop-types';
import { Button } from 'react-bootstrap';
import { Link }   from 'react-router';

const Forward = ({ onForward, nextUrl, onConfirm }) => (
  <Link to={nextUrl}>
    <Button onClick={() => {
      onForward();
      if (onConfirm !== undefined) onConfirm();
    }}
    >
      Далее
    </Button>
  </Link>
);

Forward.propTypes = {
  onForward: PropTypes.func.isRequired,
  nextUrl: PropTypes.string.isRequired,
  onConfirm: PropTypes.func
};

export default Forward;

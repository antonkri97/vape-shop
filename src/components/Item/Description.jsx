import React from 'react';
import PropTypes from 'prop-types';
import { Panel } from 'react-bootstrap';

const Description = ({ description }) => (
  <Panel header='Description' bsStyle='info'>
    {description}
  </Panel>
);

Description.propTypes = {
  description: PropTypes.string.isRequired
};

export default Description;

import React                from 'react';
import PropTypes            from 'prop-types';
import Row                  from 'react-bootstrap/lib/Row';
import Col                  from 'react-bootstrap/lib/Col';
import Description          from './Description';
import Photo                from './Photo';
import Options              from './Options';

import '../../style/item.css';

const Item = ({ item, ItemInCart, onAddToCartClick, confirmed }) => (
  <Row className='item-main'>
    <Col xs={12} md={4}>
      <Photo src={item.photo.large} />
    </Col>
    <Col mdOffset={2} md={6}>
      <h2>
        {item.name}
        <br />
        <small>
          {item.vendor}
        </small>
      </h2>
      <Description description={item.description} />
      <Options
        options={item.options}
        itemInCart={ItemInCart}
        onAddToCartClick={onAddToCartClick}
        id={item._id}
        confirmed={confirmed}
      />
    </Col>
  </Row>
);

Item.propTypes = {
  item: PropTypes.object.isRequired,
  ItemInCart: PropTypes.arrayOf(
    PropTypes.number
  ).isRequired,
  onAddToCartClick: PropTypes.func.isRequired,
  confirmed: PropTypes.bool.isRequired
};

export default Item;

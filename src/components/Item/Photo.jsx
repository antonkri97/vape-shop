import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-bootstrap';

const Photo = ({ src }) => <Image src={src} className='item-image' />;

Photo.propTypes = {
  src: PropTypes.string.isRequired
};

export default Photo;

import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Panel } from 'react-bootstrap';
import AddToCart from '../AddToCart';

const Options = ({ options, itemInCart, onAddToCartClick, confirmed, id }) => (
  <Row>
    {
      options.map((option, key) => (
        <Col xs={6} key={key}>
          <Panel>
            <li>Крепкость: {option.nicotine}</li>
            <li>Объем: {option.capacity}</li>
            <li>Type: {option.bottleType}</li>
            <li>В наличие: {option.quantity}</li>
            <li>Цена: {option.price}$</li>
            <AddToCart
              itemInCart={itemInCart}
              onAddToCartClick={onAddToCartClick}
              option={key}
              id={id}
              confirmed={confirmed}
            />
          </Panel>
        </Col>
      ))
    }
  </Row>
);

Options.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  itemInCart: PropTypes.arrayOf(PropTypes.number).isRequired,
  onAddToCartClick: PropTypes.func.isRequired,
  confirmed: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired
};

export default Options;

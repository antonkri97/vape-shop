import React                                from 'react';
import PropTypes                            from 'prop-types';
import { FormGroup, FormControl, Col, Row } from 'react-bootstrap';

const DeliveryTypes = ({ cols }) => (
  <Row>
    <Col sm={cols}>
      <FormGroup controlId='formSelect'>
        <FormControl componentClass='select' placeholder='select'>
          <option value='type 1'>s1</option>
          <option value='type 2'>s2</option>
        </FormControl>
      </FormGroup>
    </Col>
  </Row>
);

DeliveryTypes.propTypes = {
  cols: PropTypes.number
};

export default DeliveryTypes;

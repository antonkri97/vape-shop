import React                            from 'react';
import PropTypes                        from 'prop-types';
import MaskedFormControl                from 'react-bootstrap-maskedinput';
import { ControlLabel, FormGroup, Col } from 'react-bootstrap';

const Tel = ({ cols }) => (
  <Col sm={cols}>
    <FormGroup>
      <ControlLabel>Телефон</ControlLabel>
      <MaskedFormControl type='text' name='tel' mask='+1 (111)-111-11-11' />
    </FormGroup>
  </Col>
);

Tel.propTypes = {
  cols: PropTypes.number
};

export default Tel;

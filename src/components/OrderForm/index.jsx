import React      from 'react';
import PropTypes  from 'prop-types';
import FieldGroup from './FieldGroup';
import DatePicker from './Date';
import Tel        from './Tel';
import Delivery   from './DeliveryTypes';
import Promo      from './Promo';
import Back       from '../common/Back';
import Forward    from '../common/Forward';

const OrderForm = ({ onChangeOrderStageForward, onChangeOrderStageBack }) => (
  <div>
    <h4>Личные даннные</h4>
    <FieldGroup
      id='formControlFullName'
      cols={6}
      type='text'
      label='ФИО'
      placeholder='Иванов Иван Сергеевич'
    />
    <DatePicker cols={3} />
    <Tel cols={3} />
    <hr/>
    <h4>Адрес</h4>
    <FieldGroup
      id='formControlСountry'
      cols={2}
      type='text'
      label='Страна'
      placeholder='Россия'
    />
    <FieldGroup
      id='formControlCity'
      cols={2}
      type='text'
      label='Город'
      placeholder='Москва'
    />
    <FieldGroup
      id='formControlStreet'
      cols={2}
      type='text'
      label='Улица'
      placeholder='Центральная'
    />
    <FieldGroup
      id='formControlHouse'
      cols={2}
      type='number'
      label='Дом'
      placeholder='1'
    />
    <FieldGroup
      id='formControlApp'
      cols={2}
      type='number'
      label='Квартира'
      placeholder='1'
    />
    <FieldGroup
      id='formControlZipCode'
      cols={2}
      type='number'
      label='Индекс'
      placeholder='111222'
    />
    <h4>Доставка</h4>
    <Delivery cols={2} />
    <h4>Промокод</h4>
    <Promo />
    <Back onBack={onChangeOrderStageBack} nextUrl='/final' />
    <Forward onForward={onChangeOrderStageForward} nextUrl='/' />
  </div>
);

OrderForm.propTypes = {
  onChangeOrderStageBack: PropTypes.func.isRequired,
  onChangeOrderStageForward: PropTypes.func.isRequired
};

export default OrderForm;

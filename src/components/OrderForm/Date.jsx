import React, { Component }             from 'react';
import PropTypes                        from 'prop-types';
import DatePicker                       from 'react-bootstrap-date-picker';
import { FormGroup, ControlLabel, Col } from 'react-bootstrap/lib/FormGroup';

class Date extends Component {
  constructor(args) {
    super(args);

    const date = '1999-01-01T03:53:18.344Z';

    this.state = {
      value: date
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate() {
    const hiddenInputElement = document.getElementById('example-datepicker');

    console.log(hiddenInputElement.value); // ISO String, ex: '2016-11-19T12:00:00.000Z'
    console.log(hiddenInputElement.getAttribute('data-formattedvalue')); // Formatted String, ex: "11/19/2016"
  }

  handleChange(value, formattedValue) {
    this.setState({
      value,
      formattedValue
    });
  }

  render() {
    return (
      <Col sm={this.props.cols}>
        <FormGroup>
          <ControlLabel>Дата рождения</ControlLabel>
          <DatePicker id='example-datepicker' value={this.state.value} onChange={this.handleChange} />
        </FormGroup>
      </Col>
    );
  }
}

Date.propTypes = {
  cols: PropTypes.number
};

export default Date;

import React                from 'react';
import { Button, Col, Row } from 'react-bootstrap';

const style = {
  marginTop: '2%'
};

const Submit = () => (
  <Row>
    <Col sm={2} style={style} >
      <Button>
        Заказать
      </Button>
    </Col>
  </Row>
);

export default Submit;

import React      from 'react';
import { Row }    from 'react-bootstrap';
import FieldGroup from './FieldGroup';

const Promo = () => (
  <Row>
    <FieldGroup
      id='formControlPromo'
      cols={2}
      type='text'
      placeholder='LSBFREE'
    />
  </Row>
);

export default Promo;

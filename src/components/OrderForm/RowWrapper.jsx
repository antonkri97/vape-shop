import React     from 'react';
import PropTypes from 'prop-types';
import { Row }   from 'react-bootstrap';

const RowWrapper = ({ Cmp }) => <Row> <Cmp /> </Row>;

RowWrapper.propTypes = {
  Cmp: PropTypes.func.isRequired
};

export default RowWrapper;

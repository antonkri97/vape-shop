import React                          from 'react';
import PropTypes                      from 'prop-types';
import { Grid, Nav, Navbar, NavItem } from 'react-bootstrap';
import Search                         from '../Search';
import { Link }                       from 'react-router';
import LinkContainer                  from 'react-router-bootstrap/lib/LinkContainer';

import '../../style/bootstrap1.css';

const App = ({ children, cartLink, onSearch, onBrandClick }) => (
  <div>
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <Link to='/' onClick={onBrandClick}>LSB</Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav navbar>
          <LinkContainer to='/items'>
            <NavItem>Продукция</NavItem>
          </LinkContainer>
          <LinkContainer to='/delivery'>
            <NavItem>Доставка</NavItem>
          </LinkContainer>
          <LinkContainer to='/contacts'>
            <NavItem>Контакты</NavItem>
          </LinkContainer>
          <LinkContainer to='/about'>
            <NavItem>О нас</NavItem>
          </LinkContainer>
          <LinkContainer to={cartLink}>
            <NavItem><img className='cart-icon' src='https://s3.eu-central-1.amazonaws.com/vapeshopimg/icon/shopping-cart+(1).svg'/></NavItem>
          </LinkContainer>
          <Search onSearch={onSearch} />
        </Nav>
      </Navbar.Collapse>
    </Navbar>
    <Grid>
      {children}
    </Grid>
  </div>
);

App.propTypes = {
  children: PropTypes.node.isRequired,
  orderHasStarted: PropTypes.bool.isRequired,
  cartLink: PropTypes.string.isRequired,
  onSearch: PropTypes.func.isRequired,
  onBrandClick: PropTypes.func.isRequired
};

export default App;

import React     from 'react';
import PropTypes from 'prop-types';
import AppProd   from './App.prod';
import DevTools  from '../DevTools';

const App = ({ children, orderHasStarted, cartLink, onSearch, onBrandClick }) => (
  <AppProd orderHasStarted={orderHasStarted} cartLink={cartLink} onSearch={onSearch} onBrandClick={onBrandClick}>
    <div>
      {children}
      <DevTools/>
    </div>
  </AppProd>
);

App.propTypes = {
  children: PropTypes.node,
  orderHasStarted: PropTypes.bool.isRequired,
  cartLink: PropTypes.string.isRequired,
  onSearch: PropTypes.func.isRequired,
  onBrandClick: PropTypes.func.isRequired
};

export default App;

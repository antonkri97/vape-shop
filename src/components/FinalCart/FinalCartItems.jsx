import React     from 'react';
import PropTypes from 'prop-types';

const FinalCartItems = ({ items }) => (
  <ul>
    {
      items.map((i, k) => (
        <li key={k}>
          {i.name}  ({i.options[i.option].nicotine}мг, {i.options[i.option].capacity}мл) ед. {i.quantity}
        </li>
      ))
    }
  </ul>
);

FinalCartItems.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.object.isRequired
  ).isRequired
};

export default FinalCartItems;

import React          from 'react';
import PropTypes      from 'prop-types';
import Forward        from '../common/Forward';
import Back           from '../common/Back';
import FinalCartItems from './FinalCartItems';

const FinalCart = ({ items, price, onChangeOrderStageForward, onChangeOrderStageBack, nextUrl, prevUrl }) => (
  <div>
    <h4>Выбранные жидкости</h4>
    <FinalCartItems items={items} />
    <hr/>
    <h4>{price}$</h4>
    <hr/>
    <Back onBack={onChangeOrderStageBack} nextUrl={prevUrl} />
    <Forward onForward={onChangeOrderStageForward} nextUrl={nextUrl} />
  </div>
);

FinalCart.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.object.isRequired
  ).isRequired,
  price: PropTypes.number.isRequired,
  onChangeOrderStageForward: PropTypes.func.isRequired,
  onChangeOrderStageBack: PropTypes.func.isRequired,
  nextUrl: PropTypes.string.isRequired,
  prevUrl: PropTypes.string.isRequired
};

export default FinalCart;

import { connect }                    from 'react-redux';
import RowPreviewItems                from '../components/ItemsPreview';
import { showAllVendors, showVendor } from '../actions/filter';
import { filterItems, uniqueVendors } from '../utils/items';

const mapStateToProps = state => ({
  items: filterItems(state.items.data, state.filter),
  vendors: uniqueVendors(state.items.data)
});

const mapDispatchToProps = dispatch => ({
  onAllVendor: () => dispatch(showAllVendors()),
  onVendorChange: vendor => dispatch(showVendor(vendor))
});

export default connect(mapStateToProps, mapDispatchToProps)(RowPreviewItems);

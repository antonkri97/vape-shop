import Order                          from '../components/OrderForm';
import { connect }                    from 'react-redux';
import { changeStageBack, stopOrder } from '../actions/order';

const mapDispatchToProps = dispatch => ({
  onChangeOrderStageForward: () => dispatch(stopOrder()),
  onChangeOrderStageBack: () => dispatch(changeStageBack())
});

export default connect(null, mapDispatchToProps)(Order);

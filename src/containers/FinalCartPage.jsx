import FinalCart                                    from '../components/FinalCart/FinalCart';
import { connect }                                  from 'react-redux';
import { changeStageForward, changeStageBack }      from '../actions/order';
import { getFullInfoForCartProducts, getFullPrice } from '../utils/cart';

const mapStateToProps = state => {
  const cart = getFullInfoForCartProducts(state.items.data, state.cart.items);

  return {
    items: cart,
    price: getFullPrice(cart),
    nextUrl: '/order',
    prevUrl: '/cart'
  };
};

const mapDispatchToProps = dispatch => ({
  onChangeOrderStageForward: () => dispatch(changeStageForward()),
  onChangeOrderStageBack: () => dispatch(changeStageBack())
});

export default connect(mapStateToProps, mapDispatchToProps)(FinalCart);

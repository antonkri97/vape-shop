import { connect } from 'react-redux';
import App         from '../components/App/App.dev';
import { getUrl }  from '../utils/nav';
import { showSpecific, showAllVendors } from '../actions/filter';

const mapStateToProps = state => ({
  orderHasStarted: state.order.orderHasStarted,
  cartLink: getUrl(state.order.stage)
});

const mapDispatchToProps = dispatch => ({
  onBrandClick: () => dispatch(showAllVendors()),
  onSearch: value => dispatch(showSpecific(value))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

import { connect }                                    from 'react-redux';
import { addToExistingItem, removeFromCart, confirm } from '../actions/cart';
import { startOrder }                                 from '../actions/order';
import Cart                                           from '../components/Cart';
import { getFullInfoForCartProducts }                 from '../utils/cart';

const mapStateToProps = state => ({
  cart: getFullInfoForCartProducts(state.items.data, state.cart.items),
  nextUrl: '/final'
});

const mapDispatchToProps = dispatch => ({
  onAddQuantity: (id, option) => dispatch(addToExistingItem(id, option)),
  onRemoveQuantity: (id, option) => dispatch(removeFromCart(id, option)),
  onOrderStart: () => dispatch(startOrder()),
  onConfirm: () => dispatch(confirm())
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

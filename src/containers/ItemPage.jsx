import React                from 'react';
import PropTypes            from 'prop-types';
import Item                 from '../components/Item';
import { connect }          from 'react-redux';
import { addToCart }        from '../actions/cart';

const ItemPage = ({ items = [], params, onAddToCartClick, cart, confirmed }) => {
  if (items.length === 0) {
    return <div className='text-center'>loading</div>;
  }
  // находим данный продукт по id
  const item = items.find(i => i._id === params.id);
  // собираем массив, добавленных в корзину опции даного продукта
  const elementInCart = cart.reduce((prev, curr) => prev.concat(curr._id === item._id ? curr.option : []), []);

  return <Item item={item} onAddToCartClick={onAddToCartClick} ItemInCart={elementInCart} confirmed={confirmed} />;
};

ItemPage.propTypes = {
  onAddToCartClick: PropTypes.func.isRequired,
  params: PropTypes.object.isRequired,
  items: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  cart: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  confirmed: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  items: state.items.data,
  cart: state.cart.items,
  confirmed: state.cart.confirmed
});

const mapDispatchToProps = dispatch => ({
  onAddToCartClick: (id, option) => dispatch(addToCart(id, option))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemPage);

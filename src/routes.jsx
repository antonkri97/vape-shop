import React                 from 'react';
import { Route, IndexRoute } from 'react-router';
import App                   from './containers/AppPage';
import ItemsPreviewPage      from './containers/ItemsPreviewPage';
import ItemPage              from './containers/ItemPage';
import CartPage              from './containers/CartPage';
import FinalCartPage         from './containers/FinalCartPage';
import AboutPage             from './components/About';
import ContactsPage          from './components/Contacts';
import DeliveryPage          from './components/Delivery';
import OrderPage             from './containers/OrderPage';

export default (
  <Route component={App} path='/'>
    <IndexRoute component={ItemsPreviewPage} />
    <Route component={ItemsPreviewPage} path='/items'/>
    <Route component={ItemPage} path='item/:id'/>
    <Route component={CartPage} path='/cart'/>
    <Route component={FinalCartPage} path='/final' />
    <Route component={AboutPage} path='/about' />
    <Route component={ContactsPage} path='/contacts' />
    <Route component={DeliveryPage} path='/delivery' />
    <Route component={OrderPage} path='/order' />
  </Route>
);

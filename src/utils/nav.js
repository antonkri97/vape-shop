/**
 * @description возвращать путь текущего заказа
 *
 * @param { Number } stage - текущий этап заказа
 *
 * @return { String } - url
 */
export const getUrl = stage => {
  switch (stage) {
    case 0:
      return '/cart';
    case 1:
      return '/final';
    case 2:
      return '/order';
    default:
      return '/cart';
  }
};

/**
 * @description добавляет новый элемент в корзину
 *
 * @param {Object[]} cart - Корзина
 * @param {String} cart[]._id - id товара
 * @param {Number} cart[].option - Выбранная опция данного товара
 * @param {Number} cart[].quantity - Количество данного товара
 *
 * @param {Object} item - Новый товар в корзине
 * @param {String} _id - id товара
 * @param {Number} option - Опция выбранного товара
 *
 * @returns {Object} Обновленную новую корзину
 */
export const addToCart = (cart, _id, option) => cart.concat({ _id, option, quantity: 1 });

/**
 * @description увеличивает количество определенного товара в корзине на единицу
 *
 * @param {Object[]} cart - Массив товаров в корзине
 * @param {String} cart[]._id - id товара
 * @param {Number} cart[].option - Выбранная опция данного товара
 * @param {Number} cart[].quantity - Количество данного товара
 *
 * @param {String} _id - id товара
 * @param {Number} option - опция товара
 *
 * @returns {Object[]} новая корзина
 */
export const addItemQuantityInCart = (cart, _id, option) => cart.map(c => {
  if (equal(c, { _id, option })) {
    c.quantity++;
    return c;
  }
  return c;
});

/**
 * @description уменьшает количество определенного товара в корзине на единицу
 *
 * @param {Object[]} cart - Массив товаров в корзине
 * @param {String} cart[]._id - id товара
 * @param {Number} cart[].option - Выбранная опция данного товара
 * @param {Number} cart[].quantity - Количество данного товара
 *
 * @param {String} _id - id товара
 * @param {Number} option - опция товара
 *
 * @returns {Object[]} новая корзина
 */
export const removeOneItemFromCart = (cart, _id, option) => cart.reduce((prev, curr) => {
  if (equal(curr, { _id, option }) && --curr.quantity === 0) return prev;
  prev.push(curr);
  return prev;
}, []);

/**
 * @description сравнивает два элемента
 *
 * @param {Object} firstItem - первый элемент
 * @property {String} firstItem_id - id первого элемента
 * @property {Number} firstItem.option - опция первого товара
 *
 * @param {Object} secondItem - второй элемент
 * @property {String} secondItem._id - id второго элемента
 * @property {Number} secondItem.option - опция второго товара
 *
 * @returns {Boolean} true - товар с таким же id и опцией
 * @returns {Boolean} false - товары не с таким же id и опцией
 */
const equal = (firstItem, secondItem) => firstItem._id === secondItem._id && firstItem.option === secondItem.option;

/**
 * @description для каждого элемента в корзине берем полную информацию с хранилища
 *
 * @param {Object[]} fullItems все доступные товара с полной информацией
 * @param {Object[]} cartItems товары в корзине с неполной информацией
 *
 * @returns {Object[]} товары из корзины с полной информацией
 */
export const getFullInfoForCartProducts = (fullItems, cartItems) => cartItems.map(cartItem => {
  return Object.assign({}, fullItems.find(item => item._id === cartItem._id), cartItem);
});

/**
 * Получаем цену за все товары в корзине
 * @param {Object[]} items все товары в корзине
 *
 * @return {Number} конечная цена
 */
export const getFullPrice = (items) => items.reduce((prev, curr) => (
  prev + curr.options[curr.option].price * curr.quantity
  ), 0
);

/**
 * @description проверяет наличие данной опции в корзине
 *
 * @param {number} option - текущая выбранная опция на странице жидкости
 * @param {number[]} itemInCart - добавленные опции в корзину к текущей жидкости
 *
 * @returns {boolean} находится ли данная опция в корзине
 */
export const check = (option, itemInCart) => {
  if (!itemInCart.length) return false;
  return itemInCart.some(_option => {
    return _option === option;
  });
};

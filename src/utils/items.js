/**
 * @description фильтрует все продукты по текущему производителя
 *
 * @param {object[]} items - массив всех жидкостей
 * @property {string} items[].vendor - имя производителя у каждой жидкости
 *
 * @param {object} vendor - объект фильтра производителя
 * @property {string} vendor.vendorName - имя фильтра производителя
 * @property {boolean} vendor.showAll - показать всех производителей
 * @property {string} vendor.value - ключевое слово для поиска
 *
 * @returns {object[]} отфильтрованный массив жидкостей
 */
export const filterItems = (items, vendor) => {
  if (vendor.showAll) {
    return items;
  }

  if (vendor.vendorName === null && vendor.value !== null) {
    console.log('here');
    return searchItems(items, vendor.value);
  }

  return items.filter(item => item.vendor === vendor.vendorName);
};

/**
 * @description базовый поиск по 3 полям каждой жидкости
 *
 * @param {object[]} items - массив всех жидкостей
 * @param {string} value - ключевое слово для поиска
 */
const searchItems = (items, value) => items.filter(item => {
  const name = RegExp(value, 'ig').test(item.name);
  const vendor = item.vendor === value;
  const description = RegExp(value, 'ig').test(item.description);

  return name || vendor || description;
});

/**
 * @description делает массив имен производителей
 *
 * @param {object[]} items - все жидкостки
 * @property {string} items[].vendor - имя производителя жидкости
 *
 * @returns {object[]} массив имен производителей
 */
export const uniqueVendors = (items) => Array.from(new Set(items.map(item => item.vendor)));

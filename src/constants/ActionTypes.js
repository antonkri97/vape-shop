export const ITEMS_REQUEST_STARTED = 'ITEMS_REQUEST_STARTED';
export const ITEMS_REQUEST_FINISHED = 'ITEMS_REQUEST_FINISHED';
export const ITEMS_REQUEST_ERROR = 'ITEMS_REQUEST_ERROR';

export const INITIAL_CART = 'INITIAL_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const ADD_TO_EXISTING_ITEM = 'ADD_TO_EXISTING_ITEM';
export const CONFIRM_CART = 'CONFIRM_CART';
export const ADD_TO_ADDITIONAL_CART = 'ADD_TO_ADDITIONAL_CART';

export const SHOW_ALL = 'SHOW_ALL';
export const SHOW_VENDOR = 'SHOW_VENDOR';
export const SHOW_SPECIFIC = 'SHOW_SPECIFIC';

export const ORDER_HAS_STARTED = 'ORDERING_STARTED';
export const ORDER_HAS_CANCELED = 'ORDER_HAS_CANCELED';
export const CHANGE_ORDER_STAGE_FORWARD = 'CHANGE_ORDER_STAGE_FORWARD';
export const CHANGE_ORDER_STAGE_BACK = 'CHANGE_ORDER_STAGE_BACK';

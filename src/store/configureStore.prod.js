import thunk                            from 'redux-thunk';
import rootReducer                      from '../reducers';
import { applyMiddleware, createStore } from 'redux';

export default function (initialState = {}) {
  return createStore(rootReducer, initialState, applyMiddleware(thunk));
}

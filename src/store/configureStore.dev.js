import thunk                                     from 'redux-thunk';
import DevTools                                  from 'components/DevTools';
import rootReducer                               from '../reducers';
import { applyMiddleware, createStore, compose } from 'redux';
import { itemsRequest } from '../actions/items';

export default function (initialState = {}) {
  const store = createStore(rootReducer, initialState, compose(
    applyMiddleware(thunk),
    DevTools.instrument()
    )
  );

  store.dispatch(itemsRequest());

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers').default)
    );
  }

  return store;
}

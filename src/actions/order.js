import * as types from '../constants/ActionTypes';

export const startOrder = () => ({ type: types.ORDER_HAS_STARTED });

export const stopOrder = () => ({ type: types.ORDER_HAS_CANCELED });

export const changeStageForward = () => ({ type: types.CHANGE_ORDER_STAGE_FORWARD });

export const changeStageBack = () => ({ type: types.CHANGE_ORDER_STAGE_BACK });

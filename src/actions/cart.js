import * as types from '../constants/ActionTypes';

export const initialCart = cart => ({ type: types.INITIAL_CART, cart });

export const addToCart = (_id, option) => ({ type: types.ADD_TO_CART, _id, option });

export const removeFromCart = (_id, option) => ({ type: types.REMOVE_FROM_CART, _id, option });

export const addToExistingItem = (_id, option) => ({ type: types.ADD_TO_EXISTING_ITEM, _id, option });

export const confirm = () => ({ type: types.CONFIRM_CART });

export const addToAdditionalCart = (_id, option) => ({ type: types.ADD_TO_ADDITIONAL_CART, _id, option });

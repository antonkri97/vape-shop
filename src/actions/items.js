import * as types from '../constants/ActionTypes';

const  itemsRequestStarted = () => ({ type: types.ITEMS_REQUEST_STARTED });

const itemsRequestFinished = data => ({ type: types.ITEMS_REQUEST_FINISHED, data });

const itemsRequestError = ex => ({ type: types.ITEMS_REQUEST_ERROR, ex });

export const itemsRequest = () => async dispatch => {
  dispatch(itemsRequestStarted());
  try {
    const req = await fetch('http://localhost:8080/items');

    if (req.status !== 200) dispatch(itemsRequestError(req.statusText));

    dispatch(itemsRequestFinished(await req.json()));
  } catch (error) {
    dispatch(itemsRequestError(error.data.message));
  }
};

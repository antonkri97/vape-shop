import { SHOW_ALL, SHOW_VENDOR, SHOW_SPECIFIC } from '../constants/ActionTypes';

export const showAllVendors = () => ({ type: SHOW_ALL });

export const showVendor = (vendor) => ({ type: SHOW_VENDOR, vendor });

export const showSpecific = (value) => ({ type: SHOW_SPECIFIC, value });

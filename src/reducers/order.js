import * as types from '../constants/ActionTypes';

const initialState = {
  orderHasStarted: false,
  stage: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.ORDER_HAS_STARTED:
      return { orderHasStarted: true, stage: 1 };
    case types.ORDER_HAS_CANCELED:
      return { orderHasStarted: false, stage: 0 };
    case types.CHANGE_ORDER_STAGE_FORWARD:
      return Object.assign({}, state, { stage: state.stage + 1 });
    case types.CHANGE_ORDER_STAGE_BACK:
      return Object.assign({}, state, { stage: state.stage - 1 });
    default:
      return state;
  }
};

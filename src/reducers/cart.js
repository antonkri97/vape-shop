import * as cart  from '../utils/cart';
import * as types from '../constants/ActionTypes';

const initialState = {
  items: [],
  confirmed: false,
  additional: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_TO_CART:
      return { items: cart.addToCart(state.items, action._id, action.option) };
    case types.ADD_TO_EXISTING_ITEM:
      return { items: cart.addItemQuantityInCart(state.items, action._id, action.option) };
    case types.REMOVE_FROM_CART:
      return { items: cart.removeOneItemFromCart(state.items, action._id, action.option) };
    case types.CONFIRM_CART:
      return Object.assign({}, state, { confirm: true });
    default:
      return state;
  }
};

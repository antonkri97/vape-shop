import { SHOW_ALL, SHOW_VENDOR, SHOW_SPECIFIC } from '../constants/ActionTypes';

const initialState = {
  showAll: true,
  vendorName: null,
  value: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_ALL:
      return Object.assign({}, state, { showAll: true });
    case SHOW_VENDOR:
      return Object.assign({}, state, { showAll: false, vendorName: action.vendor });
    case SHOW_SPECIFIC:
      return Object.assign({}, state, { value: action.value, showAll: false, vendorName: null });
    default:
      return state;
  }
}

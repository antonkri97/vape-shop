import * as types from '../constants/ActionTypes';

const initialState = {
  data: [],
  errors: null,
  loading: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case types.ITEMS_REQUEST_STARTED:
      return Object.assign({}, state, { loading: true, errors: null });
    case types.ITEMS_REQUEST_FINISHED:
      return {
        loading: false,
        errors: null,
        data: action.data
      };
    case types.ITEMS_REQUEST_ERROR:
      return Object.assign({}, state, { loading: false, errors: action.errors });
    default:
      return state;
  }
}

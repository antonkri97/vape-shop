import cart                from './cart';
import order               from './order';
import items               from './items';
import filter              from './filter';
import { combineReducers } from 'redux';

export default combineReducers({
  items,
  cart,
  filter,
  order
});
